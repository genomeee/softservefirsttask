--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.24
-- Dumped by pg_dump version 9.3.24
-- Started on 2018-11-22 12:14:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1969 (class 0 OID 16744)
-- Dependencies: 173
-- Data for Name: doctors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.doctors (id, first_name, last_name, start_time, end_time) FROM stdin;
20	Kurt	Cobain	09:00:00	18:00:00
29	Jonathan	Dorian	09:00:00	18:00:00
30	Christopher	Turk	09:00:00	18:00:00
31	Gregory	House	09:00:00	20:00:00
21	Percival	Cox	07:00:00	23:00:00
\.


--
-- TOC entry 1970 (class 0 OID 16754)
-- Dependencies: 174
-- Data for Name: patients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.patients (id, first_name, last_name, date_of_birth, phone) FROM stdin;
32	Olexsandr	Pridolob	1998-12-01	+380507777866
33	Selim	Abdul	1998-12-30	+380506666744
34	Jonh	Wick	1975-05-14	+380501111233
\.


--
-- TOC entry 1968 (class 0 OID 16689)
-- Dependencies: 172
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name) FROM stdin;
1	DOCTOR
2	PATIENT
3	ADMIN
\.


--
-- TOC entry 1980 (class 0 OID 0)
-- Dependencies: 171
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, false);


--
-- TOC entry 1972 (class 0 OID 16772)
-- Dependencies: 176
-- Data for Name: timeslots; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.timeslots (id, id_doctor, id_patient, start_time, end_time) FROM stdin;
57	29	34	12:00:00	12:15:00
58	31	34	15:00:00	16:00:00
59	21	34	17:00:00	18:00:00
\.


--
-- TOC entry 1981 (class 0 OID 0)
-- Dependencies: 175
-- Name: timeslots_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.timeslots_id_seq', 59, true);


--
-- TOC entry 1974 (class 0 OID 16791)
-- Dependencies: 178
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, email, password, role) FROM stdin;
20	mishasydora@gmail.com	$2a$15$rSKU4LAIrEdKsUL3Jui7tubwV.PCePOr79MDjrvjALdv9nF8BKTrC                                                                                                                                                                                                   	1
26	admin@gmail.com	$2a$15$bU7Z1iXCRtdZ.w6QIjULPO.0Ov3WKXnwEdQVIKPrSH7hf2jZQhipS                                                                                                                                                                                                   	3
29	jd@gmail.com	$2a$15$i0OBzAajXCJiN1ZDVM6Z0ObKWMI5gM.LVEpuUnGevrfjkUVU9yupm                                                                                                                                                                                                   	1
30	ct@gmail.com	$2a$15$qBU8Euz.aaZmrb9ZW5kUcOM/TVhT3SiirKEfzkvKTghlehVZeyW7i                                                                                                                                                                                                   	1
31	house@gmail.com	$2a$15$B2pq5.ba1199UULYVYwfNuO2h8KB6QowSoyU2C40EBdzN1CsEtd4G                                                                                                                                                                                                   	1
32	op@gmail.com	$2a$15$tlddiTi64UkzZ1OU3MGru.J1vN/KhBSeN0uLT5xoQ6.qVb5V9DgdC                                                                                                                                                                                                   	2
33	sa@gmail.com	$2a$15$RODZn98HoN1JHwyOfD/mhunTCXm9QpYzXno1HooovALfca7GiXjVC                                                                                                                                                                                                   	2
34	jw@gmail.com	$2a$15$QY/.dkbMF/xXTdkWmlYsiOjYSGAe4CXgRhQYk2Nsl7ftoKKzr.cUS                                                                                                                                                                                                   	2
21	cox@gmail.com	$2a$15$O6a8WzR07cvHhLHArZaGluFQOLvUP2AWGpr0n4KDkR0opFq7xnu0O                                                                                                                                                                                                   	1
\.


--
-- TOC entry 1982 (class 0 OID 0)
-- Dependencies: 177
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 44, true);


-- Completed on 2018-11-22 12:14:18

--
-- PostgreSQL database dump complete
--

