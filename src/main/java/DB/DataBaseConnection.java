package DB;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class DataBaseConnection {

    private Connection con = null;

    private static DataBaseConnection dataBaseConnection = null;

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (dataBaseConnection == null){
            dataBaseConnection = new DataBaseConnection();
        }
        return dataBaseConnection.getCon();
    }

    private void init() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        String propFileName = "config.properties";

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        Properties prop = new Properties();

        try {
            prop.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


        String url = prop.getProperty("database.url");
        String login = prop.getProperty("database.user");
        String password = prop.getProperty("database.password");
        con = DriverManager.getConnection(url,login,password);
    }

    private Connection getCon() {
        return con;
    }

    private DataBaseConnection() throws SQLException, ClassNotFoundException {
        init();
    }
}