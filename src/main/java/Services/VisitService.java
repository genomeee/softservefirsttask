package Services;

import exeptions.AddRecordExeption;
import models.Doctor;
import models.TimeSlot;
import org.apache.log4j.Logger;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


public class VisitService {


    private static Logger logger = Logger.getLogger(VisitService.class);


    public boolean patientSearchTime(Doctor doctor, int intervall) throws AddRecordExeption {
        LocalTime temp;

        LocalTime localTime = doctor.getStartTime();
        do {
            TimeSlot timeSlot = new TimeSlot();
            temp = localTime;
            timeSlot.setFrom(localTime);
            temp = temp.plusMinutes(intervall);
            timeSlot.setTo(temp);
            if (doctor.isCanInsert(timeSlot)) {
                doctor.addToRecords(timeSlot);
                logger.info("Patient get slot: " + timeSlot);
                return true;
            }
            localTime = temp;
        } while (localTime.isBefore(doctor.getEndTime()));
        logger.warn("Doctor busy today");
        return false;

    }

    public boolean patientSearchTime(Doctor doctor, LocalTime startTime, int intervall) throws AddRecordExeption {
        LocalTime temp;
        LocalTime localTime = startTime;
        do {
            TimeSlot timeSlot = new TimeSlot();
            temp = localTime;
            timeSlot.setFrom(localTime);
            temp = temp.plusMinutes(intervall);
            timeSlot.setTo(temp);
            if (doctor.isCanInsert(timeSlot)) {
                doctor.addToRecords(timeSlot);
                logger.info("Patient get slot: " + timeSlot);
                return true;
            }
            localTime = temp;
        } while (startTime.isBefore(doctor.getEndTime()));
        logger.warn("Doctor busy today");
        return false;
    }


    public boolean patientSearchTime(Doctor doctor, TimeSlot timeSlot, int intervall) throws AddRecordExeption {
        LocalTime temp;
        LocalTime localTime = timeSlot.getFrom();
        do {
            TimeSlot timeSlot2 = new TimeSlot();
            temp = localTime;
            timeSlot2.setFrom(localTime);
            temp = temp.plusMinutes(intervall);
            timeSlot2.setTo(temp);
            if (doctor.isCanInsert(timeSlot2)) {
                doctor.addToRecords(timeSlot2);
                logger.info("Patient get slot: " + timeSlot);
                return true;
            }
            localTime = temp;
        } while (localTime.isBefore(timeSlot.getTo()));
        logger.warn("Doctor busy today");
        return false;
    }


    public List<TimeSlot> getAllFreeInterval(Doctor doctor, int intervall) {
        LocalTime localTime = doctor.getStartTime();
        LocalTime localTime1 = doctor.getEndTime();
        List<TimeSlot> timeSlots = new ArrayList<>();

        LocalTime temp;
        do {
            TimeSlot timeSlot = new TimeSlot();
            temp = localTime;
            timeSlot.setFrom(localTime);
            temp = temp.plusMinutes(intervall);
            timeSlot.setTo(temp);
            if (doctor.isCanInsert(timeSlot)) {
                timeSlots.add(timeSlot);
            }
            localTime = temp;
        } while (localTime.isBefore(localTime1));
        logger.info("Free time today: " + timeSlots);
        return timeSlots;

    }

    public void addTimeSlot(Doctor doctor, TimeSlot timeSlot) throws AddRecordExeption {
        if (doctor.isCanInsert(timeSlot)) {
            doctor.getTimeList().add(timeSlot);
            doctor.getTimeList().sort(TimeSlot.COMPARE_BY_FROM);
        } else {
            throw new AddRecordExeption("Лікар не може в цей час");
        }

    }

    public boolean addTimeSlotPatient(List<TimeSlot> timeSlots, TimeSlot timeSlot) {
        logger.info("Add record\n" + timeSlot.toString());
        if (timeSlots.isEmpty()) {
            logger.info("Successful add at " + 0 + " position");
            return true;
        }
        if (timeSlot.getTo().isBefore(timeSlots.get(0).getFrom().plusMinutes(1))) {
            logger.info("Successful add at " + 0 + " position");
            return true;
        }
        if (timeSlot.getTo().isAfter(timeSlots.get(timeSlots.size() - 1).getTo().minusMinutes(1))) {
            logger.info("Successful add at " + 1 + " position");
            return true;
        }
        for (int i = 0; (i < timeSlots.size() - 1); i++) {
            if ((timeSlot.getFrom().isAfter(timeSlots.get(i).getTo()) && timeSlot.getTo().isBefore(timeSlots.get(i + 1).getFrom()))
                    || (timeSlot.getFrom().isAfter(timeSlots.get(timeSlots.size() - 1).getTo()))) {
                return true;
            }
        }
        return false;
    }
}