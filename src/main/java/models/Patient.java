package models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Objects;

public class Patient extends User {
    private static final String PHONE_REGEX = "^\\+380\\d{3}\\d{2}\\d{2}\\d{2}$";


    @NotNull(message = "Date of Birth cannot be null")
    private LocalDate birthDate;
    @Pattern(regexp = PHONE_REGEX)
    @NotNull(message = "Phone cannot be null")
    private String phone;
    @NotNull(message = "Date of Birth cannot be null")
    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(birthDate, patient.birthDate) &&
                Objects.equals(phone, patient.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), birthDate, phone);
    }

    @Override
    public String toString() {
        return "Patient{" + "id=" + getId()+
                ", birthDate=" + birthDate +
                ", phone='" + phone + '\'' +
                '}';
    }
}
