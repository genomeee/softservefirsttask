package models;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class Role {
    private static final String DEFAULT_PATIENT_ROLE = "PATIENT";
    private static final String DOCTOR_ROLE = "DOCTOR";
    private static final String ADMIN_ROLE = "ADMIN";

    @Min(1)
    @Positive
    private int roleId;

    @NotNull
    private String name;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role() {
        roleId = 2;
        name = DEFAULT_PATIENT_ROLE;
    }

    public boolean isRolePatient() {
        return DEFAULT_PATIENT_ROLE.equals(name);
    }

    public boolean isRoleDoctor() {
        return DOCTOR_ROLE.equals(name);
    }
    public boolean isRoleAdmin(){
        return ADMIN_ROLE.equals(name);
    }
    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                ", name='" + name + '\'' +
                '}';
    }
}

