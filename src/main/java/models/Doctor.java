package models;

import exeptions.AddRecordExeption;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@validator.Doctor
public class Doctor extends User {

    @NotNull(message = "Start time cannot be null")
    private LocalTime startTime;
    @NotNull(message = "End time cannot be null")
    private LocalTime endTime;
    private List<TimeSlot> timeList = new ArrayList<>();


    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public List<TimeSlot> getTimeList() {
        return timeList;
    }

    public void setTimeList(List<TimeSlot> timeList) {
        this.timeList = timeList;
    }



    public boolean isCanInsert(TimeSlot timeSlot){
        boolean flag = false;
            if ((timeSlot.getFrom().plusMinutes(1).isAfter(startTime))
                    && (timeSlot.getTo().plusMinutes(1).isBefore(endTime)) && (timeSlot.getTo().isAfter(startTime))) {
                if (timeList.isEmpty()) {
                    flag = true;
                } else if ((timeList.size() == 1) && ((timeSlot.getTo().isBefore(timeList.get(0).getFrom())))
                        || (timeSlot.getFrom().isAfter(timeList.get(timeList.size() - 1).getTo()))) {
                    flag = true;
                } else {
                    for (int i = 0; (i < timeList.size() - 1) && !flag; i++) {
                        if ((timeSlot.getTo().isBefore(timeList.get(0).getFrom()))
                                || (timeSlot.getFrom().isAfter(timeList.get(i).getTo()) && timeSlot.getTo().isBefore(timeList.get(i + 1).getFrom()))
                                || (timeSlot.getFrom().isAfter(timeList.get(timeList.size() - 1).getTo()))) {
                            flag = true;
                        }
                    }
                }
            } else {
            flag = false;
            }
        return flag;
    }
    public void addToRecords(TimeSlot timeSlot) throws AddRecordExeption {
        if(isCanInsert(timeSlot)){
            timeList.add(timeSlot);
            timeList.sort(TimeSlot.COMPARE_BY_FROM);
        }
        else {
            throw new AddRecordExeption("Лікар не може в цей час");
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(startTime, doctor.startTime) &&
                Objects.equals(endTime, doctor.endTime) &&
                Objects.equals(timeList, doctor.timeList);
    }




    @Override
    public int hashCode() {
        return Objects.hash(startTime, endTime, timeList);
    }


    @Override
    public String toString() {
        return "Doctor{" +
                "id="+getId()+
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", timeList=" + timeList +
                '}';
    }
}

