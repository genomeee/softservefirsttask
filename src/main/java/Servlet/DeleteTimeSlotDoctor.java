package Servlet;

import Services.DoctorService;
import Services.TimeSlotService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.Doctor;
import models.TimeSlot;
import models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "DeleteTimeSlotDoctor", urlPatterns = {"/deleteTimeSlotDoctor"})
public class DeleteTimeSlotDoctor extends HttpServlet {
    private final Logger logger = LogManager.getLogger(DeleteTimeSlotController.class);
    private TimeSlotService timeSlotService = new TimeSlotService();
    private DoctorService doctorService = new DoctorService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        logger.info("Try to delete timeSlot");
        String currentPass = request.getParameter("password");
        Long id = Long.parseLong(request.getParameter("idDoctor"));
        List<String> errors = null;
        try {
            User user = (User) request.getSession().getAttribute("User");
            TimeSlot timeSlot = timeSlotService.findByid(id);
            timeSlot.setId(id);
            Doctor doctor = doctorService.findById(user.getId());
            logger.info("Try to delete timeSlot : " + timeSlot.toString());
            errors = timeSlotService.deleteTimeSlotDoctor(timeSlot,doctor, currentPass);
            if (!errors.isEmpty()) {
                logger.error("Change password error" + errors);
                request.setAttribute("errors", errors);
                request.setAttribute("doctor", doctor);
                getServletContext().getRequestDispatcher("DoctorPersonalPage.jsp").forward(request, response);
                return;
            }
            logger.info("Successful delete");
            response.sendRedirect("DoctorPersonalPage.jsp");

        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (DependencyExeption | AddRecordExeption dependencyExeption) {
            dependencyExeption.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
