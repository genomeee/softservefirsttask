package Servlet;

import Services.PatientService;
import Services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "reg", urlPatterns = {"/reg"})

public class RegistrationServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("reg.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String phone=request.getParameter("phone");
        String login=request.getParameter("email");

        String password=request.getParameter("password");
        String confirmPassword=request.getParameter("confirmPass");

        LocalDate date= LocalDate.parse(request.getParameter("birthday"));
        List<String> errors;
        try {
            errors = new PatientService().registerPatient(firstName,lastName,date,phone,login,password,confirmPassword);
            if(!errors.isEmpty()) {
                request.setAttribute("registerError", errors);
                getServletContext().getRequestDispatcher("login.jsp").forward(request, response);
                return;
            }
            HttpSession session = request.getSession();
            session.setAttribute("User", new UserService().getUserFromDB(login));
            session.setMaxInactiveInterval(300);
            getServletContext().getRequestDispatcher("/PatientPersonalPage.jsp").forward(request, response);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}