package Servlet.admin;

import Services.TimeSlotService;
import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.TimeSlot;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "AdminDeleteTimeSlot", urlPatterns = {"/adminDeleteTimeSlot"})
public class AdminDeleteTimeSlot extends HttpServlet {
    private final Logger logger = LogManager.getLogger( AdminDeleteDoctor.class);
    private TimeSlotService timeSlotService = new TimeSlotService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Long id = Long.parseLong(request.getParameter("idTimeSlot"));
        logger.info("Try to delete timeSlot");
        try {
            TimeSlot timeSlot = timeSlotService.findByid(id);

            logger.info("Try to delete timeSlot : "+ timeSlot.toString());
            timeSlotService.adminDelete(id);
            logger.info("Successful delete");
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() +"/admin"));

        } catch (SQLException | ClassNotFoundException | AddRecordExeption e) {
            logger.error(e.getMessage());
        } catch (DependencyExeption dependencyExeption) {
            dependencyExeption.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
