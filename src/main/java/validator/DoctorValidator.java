package validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalTime;

public class DoctorValidator implements ConstraintValidator<Doctor, models.Doctor> {
    public void initialize(Doctor constraintAnnotation) {
    }

    @Override
    public boolean isValid(models.Doctor doctor,ConstraintValidatorContext constraintValidatorContext)  {
        LocalTime startTime = doctor.getStartTime();
        LocalTime endTime = doctor.getEndTime();
        if (startTime.isBefore(endTime)) {
            return true;
        }
        return false;
    }
}
