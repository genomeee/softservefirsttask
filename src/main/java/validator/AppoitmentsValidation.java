package validator;

import models.Doctor;
import models.Patient;
import models.TimeSlot;
import models.User;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;




public class AppoitmentsValidation {
    private Validator validator;
    private List<String> errorMessages;


    public AppoitmentsValidation() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        errorMessages = new ArrayList<>();
    }

    public List<String> validate(User user) {
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        for(ConstraintViolation<User> violation : violations) {
            errorMessages.add(violation.getMessage());
        }

        return errorMessages;
    }

    public List<String> validate(Patient patient)
    {
        Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
        for (ConstraintViolation<Patient> violation : violations) {
            errorMessages.add(violation.getMessage());
        }
        return errorMessages;
    }

    public List<String> validate(Doctor doctor){
        Set<ConstraintViolation<Doctor>> violations = validator.validate(doctor);
        for (ConstraintViolation<Doctor> violation : violations) {
            errorMessages.add(violation.getMessage());
        }
        return errorMessages;
    }

    public List<String> validate(TimeSlot timeSlot) {
        Set<ConstraintViolation<TimeSlot>> violations = validator.validate(timeSlot);
        for(ConstraintViolation<TimeSlot> violation : violations) {
            errorMessages.add(violation.getMessage());
        }

        return errorMessages;
    }


}