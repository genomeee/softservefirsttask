package validator;

import org.joda.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateBirthValidator implements ConstraintValidator<DateBirth, LocalDate> {

    private LocalDate minDate;
    private LocalDate maxDate;

    @Override
    public void initialize(DateBirth constraintAnnotation) {
        LocalDate now=LocalDate.now();
        int yearsBeforeNow = 120;
        this.minDate = now.minusYears(yearsBeforeNow);
        int daysBeforeNow = 1;
        this.maxDate=now.minusDays(daysBeforeNow);
    }
    @Override
    public boolean isValid(LocalDate date, ConstraintValidatorContext constraintValidatorContext) {
        return (date.isAfter(minDate)&& date.isBefore(maxDate));
    }
}