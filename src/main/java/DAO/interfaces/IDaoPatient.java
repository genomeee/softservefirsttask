package DAO.interfaces;

import exeptions.DependencyExeption;
import models.Patient;

import java.sql.SQLException;
import java.util.List;

public interface IDaoPatient {

    Patient findById(Long id) throws SQLException, ClassNotFoundException, DependencyExeption;

    List<Patient> findAll() throws SQLException, ClassNotFoundException;

    void delete(Long id) throws SQLException, ClassNotFoundException, DependencyExeption;

    void save(Patient patient) throws SQLException, ClassNotFoundException;

    void update(Patient patient) throws SQLException, ClassNotFoundException;
}
