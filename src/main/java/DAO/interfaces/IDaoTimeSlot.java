package DAO.interfaces;

import exeptions.AddRecordExeption;
import exeptions.DependencyExeption;
import models.TimeSlot;

import java.sql.SQLException;
import java.util.List;

public interface IDaoTimeSlot {
    TimeSlot findById(Long id) throws SQLException, ClassNotFoundException, DependencyExeption, AddRecordExeption;

    List<TimeSlot> findAll() throws SQLException, ClassNotFoundException;

    void delete(Long id) throws SQLException, ClassNotFoundException;

    void save(TimeSlot timeSlot) throws SQLException, ClassNotFoundException;

    void update(TimeSlot timeSlot) throws SQLException, ClassNotFoundException;
}
