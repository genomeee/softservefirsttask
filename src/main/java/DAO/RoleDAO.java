package DAO;

import DB.DataBaseConnection;
import models.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleDAO {

    private static final String ROLE_NAME_COLUMN_NAME = "name";
    /* SQL requests */
    private static final String GET_ROLE_BY_ID_REQUEST = "SELECT * FROM roles WHERE id=?";


    public Role getRoleById(int id) throws SQLException, ClassNotFoundException {
        Connection connection = DataBaseConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_ROLE_BY_ID_REQUEST);

        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        Role role = null;

        if (resultSet.next()) {
            role = new Role();
            role.setRoleId(id);
            role.setName(resultSet.getString(ROLE_NAME_COLUMN_NAME));
        }
//        resultSet.close();
//        preparedStatement.close();

        return role;
    }


}
