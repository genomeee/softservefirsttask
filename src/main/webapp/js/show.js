$(document).ready(function() {
    $('.list-group-item').fadeOut();
    $('button#show').click(function(){
        $('.list-group-item').fadeIn();
    });
    $('button#hide').click(function(){
        $('.list-group-item').fadeOut();
    });
});